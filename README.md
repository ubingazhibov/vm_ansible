# VM и Ansible
## Задание 1
1. Я решил использовать Vagrant, для создания virtualbox. Vagrant - продукт компании HashiCorp, который позволяет в качестве оболочки управлять системами виртуализации.
То есть у нас под капотом может быть любой гипервизер, но через vagrant можно управлять ими одними и теми же командами, что сильно облягчает нам работу <br>
Скачаем Vagrant.
   ```
   curl -fsSL https://apt.releases.hashicorp.com/gpg | sudo apt-key add -
   sudo apt-add-repository "deb [arch=amd64] https://apt.releases.hashicorp.com $(lsb_release -cs) main"
   sudo apt-get update && sudo apt-get install vagrant
   ```
2. Пишем Vagrantfile, где создаем 2 virutalbox на базе ubuntu2204 - **ansible-vm** и **db-vm**. Документация vagrant очень понятная, написал vagrantfile с помощью [это](https://developer.hashicorp.com/vagrant/docs/multi-machine)
, [это](https://developer.hashicorp.com/vagrant/docs/networking/private_network) и [это](https://developer.hashicorp.com/vagrant/docs/provisioning/shell).
   **run_ansible.sh** запускается при создании ansible-vm, чтобы туда скачать ansible. 
3. Потом запускаем vagrant 
   ```
   vagrant up
   ``` 
4. Проверяем статус машин. 
   ```   
   vagrant status
   ```
   **Смотрите скриншот - ./screenshots/vm_status.png** <br>    
5. Заходим в db-vm и узнаем айпи адрес этого сервера
   ```
   vagrant ssh db-vm #заходим на сервер через vagrant ssh
   ip addr # узнаем айпи адрес
   ```
   Допустим, айпи адрес **db-vm = 192.168.56.31** <br>
   **Смотрите скриншот - ./screenshots/id_addr_db-vm.png**      <br>  
6. Заходим в ansible-vm и создаем ключи rsa и перебросим public key в db-vm(на предыдущем шаге мы узнал его айпи адрес)
   ```
   vagrant ssh ansible-vm #заходим в ansible vm
   ssh-keygen -q -t rsa -N '' -f ~/.ssh/id_rsa <<<y >/dev/null 2>&1 # создаем ключи rsa
   ssh-copy-id -i $HOME/.ssh/id_rsa.pub vagrant@192.168.56.31   #перебрасываем публичные ключи в db-vm
   ```
   **Смотрите скриншот - ./screenshots/generating_keys.png**      <br>
   P.S Vagrant создает vm с юзером vagrant. <br>
7. Теперь с ansible-vm можем заходить в db-vm через **ssh vagrant@192.168.56.31**, где vagrant - username, 192.168.56.31 - ip address db-vm. <br>
**Смотрите скриншот - ./screenshots/generating_keys.png**
   <br>
## Задание 2

1. Добавляем в файл **hosts**, который находится в ansible-vm айпи адрес db-vm. То есть:
   ```
   [my_hosts]
   192.168.56.31
   ```
2. Проверяем можем ли подключиться с ansible-dm в db-vm с помощью ansible ping
   ```
   ansible my_hosts -m ping -i hosts
   ```
   **Смотрите скриншот - ./screenshots/check_ansible_hosts.png**  <br>
3. Создаем ansible файл (**playbook.yml**), для скачивания postgresql 16. 
4. Запускаем ansible файл в ansible-vm, чтобы загрузить postresql 16 в db-vm:
   ```
   ansible-playbook -i hosts playbook.yml
   ```
   **Смотрите скриншот - ./screenshots/ansible_playbook_running.png**   <br>
5. Теперь у нас есть postgres 16 в db-vm. Cоздадим там юзера hardcode_db_user и пустую базу данных hardcode_db.
   ```  
   vagrant ssh db-vm
   sudo -i -u postgres
   psql
   CREATE USER hardcode_db_user;
   CREATE DATABASE hardcode_db OWNER hardcode_db_user;
   ```
   **Смотрите скриншот - ./screenshots/working_with_postgres.png** <br>
6. На этом все. Если vm больше не нужен:
   ```
   vagrant destroy -f
   ```

## Вы просили
- Код ansible - В **playbook.yml**, 
- SQL команды - **задание 2, шаг 5**. Но юзера создал без пароля, можно добавить .. WITH PASSWORD 'pass' 
-   cкриншот команд для вывода статуса PostgreSQL сервера - **vagrant status db-vm**, скриншот **./screenshots/vm_status.png**
-   скриншот версии PostgreSQL - **./screenshots/ansible_playbook_running.png**
# Автор
 Аслан Убингажибов - aslan.ubingazhibov@alumni.nu.edu.kz, telegram - https://t.me/ubingazhibov